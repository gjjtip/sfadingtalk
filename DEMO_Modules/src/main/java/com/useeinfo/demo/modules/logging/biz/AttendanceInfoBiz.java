package com.useeinfo.demo.modules.logging.biz;

import com.useeinfo.demo.modules.logging.dao.AttendanceInfoDao;
import com.useeinfo.demo.modules.logging.entity.AttendanceInfo;
import com.useeinfo.framework.extend.biz.CrudBiz;
import com.useeinfo.framework.sugar.data.QueryParam;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author: 龚俊杰
 */
@Service("attendanceInfoBiz")
public class AttendanceInfoBiz extends CrudBiz<AttendanceInfoDao, AttendanceInfo> {

    public AttendanceInfo findAttendanceInfo(String userTel, String shopName) {
        QueryParam queryParam = new QueryParam();
        queryParam.getSqlMap().put("shopName", shopName);
        queryParam.getSqlMap().put("userTel", userTel);
        List<AttendanceInfo> attendanceInfoList = dao.findList(queryParam);
        if (CollectionUtils.isNotEmpty(attendanceInfoList))
            return attendanceInfoList.get(0);
        return null;
    }

    public List<AttendanceInfo> findAttendance(String tel, String storeName) {
        return this.dao.findAttendance(tel, storeName);
    }
}
