package com.useeinfo.demo.modules.logging.biz;


import com.useeinfo.demo.modules.logging.dao.FeedBackInfoDao;
import com.useeinfo.demo.modules.logging.entity.FeedBackInfo;
import com.useeinfo.framework.extend.biz.CrudBiz;
import org.springframework.stereotype.Service;

/**
 * Created by chentao on 2017/5/5.
 */
@Service("feedBackInfoBiz")
public class FeedBackInfoBiz extends CrudBiz<FeedBackInfoDao, FeedBackInfo> {
}
