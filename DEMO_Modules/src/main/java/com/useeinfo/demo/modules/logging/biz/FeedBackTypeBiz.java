package com.useeinfo.demo.modules.logging.biz;


import com.useeinfo.demo.modules.logging.dao.FeedBackTypeDao;
import com.useeinfo.demo.modules.logging.entity.FeedBackType;
import com.useeinfo.framework.extend.biz.CrudBiz;
import org.springframework.stereotype.Service;

/**
 * Created by chentao on 2017/5/5.
 */
@Service("feedBackTypeBiz")
public class FeedBackTypeBiz extends CrudBiz<FeedBackTypeDao, FeedBackType> {
}
