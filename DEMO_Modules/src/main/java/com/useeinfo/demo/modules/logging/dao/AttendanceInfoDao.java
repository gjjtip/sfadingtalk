package com.useeinfo.demo.modules.logging.dao;

import com.useeinfo.demo.modules.logging.entity.AttendanceInfo;
import com.useeinfo.demo.modules.logging.entity.VisitInfo;
import com.useeinfo.framework.extend.dao.BaseDao;
import com.useeinfo.framework.extend.dao.CrudDao;
import com.useeinfo.framework.sugar.data.QueryParam;
import com.useeinfo.framework.sugar.data.QueryUtils;
import com.useeinfo.framework.sugar.tools.CommonSugar;
import com.useeinfo.framework.sugar.tools.StringConverters;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: 龚俊杰
 */
@Repository
public class AttendanceInfoDao implements CrudDao<AttendanceInfo> {

    private static final Logger logger = LoggerFactory.getLogger(AttendanceInfoDao.class);

    @PersistenceContext
    private EntityManager entityManager;

    public Map<String, Object> getSearchCondition(Map<String, String> queryHash) {

        Map<String, Object> conditionHash = new LinkedHashMap<>();
        if (queryHash == null) {
            return conditionHash;
        }

        String userName = queryHash.get("userName");
        if (!StringUtils.isEmpty(userName)) {
            conditionHash.put("userName = ?{paramIndex} ", userName);
        }
        Date dutyDate = StringConverters.ToDateOnly(queryHash.get("dutyDate"));
        if (dutyDate != null) {
            conditionHash.put("dutyDate = ?{paramIndex} ", dutyDate);
        }

        return conditionHash;
    }

    @Override
    public Long totalRecord(Map<String, String> queryHash) {

        Map<String, Object> conditions = getSearchCondition(queryHash);
        TypedQuery<Long> typedQuery = QueryUtils.getTypedQueryByCondition("select count(a) from AttendanceInfo a ", conditions, entityManager, Long.class);
        return typedQuery.getSingleResult();
    }

    @Override
    public List<AttendanceInfo> findList(QueryParam queryParam) {

        String sqlOrder = CommonSugar.getStringDefault(queryParam.getSqlOrder(), "order by a.id desc ");
        String sqlInfo = "select a from AttendanceInfo a " + sqlOrder;

        Map<String, Object> conditions = getSearchCondition(queryParam.getSqlMap());
        TypedQuery<AttendanceInfo> typedQuery = QueryUtils.getTypedQueryByCondition(sqlInfo, conditions, entityManager, AttendanceInfo.class);

        //设定分页信息后返回数据
        return queryParam.findPageList(typedQuery);
    }

    @Override
    public AttendanceInfo findModel(Long id) {
        return entityManager.find(AttendanceInfo.class, id);
    }

    @Override
    public Integer add(AttendanceInfo attendanceInfo) {
        attendanceInfo.setCreateDate(new Date());
        entityManager.persist(attendanceInfo);
        logger.info("AttendanceInfoDaoImpl添加attendanceInfo成功！");
        return 1;
    }

    @Override
    public Integer update(AttendanceInfo attendanceInfo) {
        AttendanceInfo existAttendanceInfo = entityManager.find(AttendanceInfo.class, attendanceInfo.getId());
        existAttendanceInfo.setShopName(attendanceInfo.getShopName());
        existAttendanceInfo.setDutyDate(attendanceInfo.getDutyDate());
        existAttendanceInfo.setInExceptionId(attendanceInfo.getInExceptionId());
        existAttendanceInfo.setOutExceptionId(attendanceInfo.getOutExceptionId());
        existAttendanceInfo.setOutDutyTime(new Date());
        existAttendanceInfo.setOutLatitude(attendanceInfo.getOutLatitude());
        existAttendanceInfo.setOutDutyPhoto(attendanceInfo.getOutDutyPhoto());
        existAttendanceInfo.setOutLongitude(attendanceInfo.getOutLongitude());
        existAttendanceInfo.setUpdateDate(new Date());
        return 1;
    }

    @Override
    public Integer delete(Long id) {
        AttendanceInfo existAttendanceInfo = entityManager.find(AttendanceInfo.class, id);
        entityManager.remove(existAttendanceInfo);
        return 1;
    }

    public List<AttendanceInfo> findAttendance(String tel, String storeName) {
        String sql = "select * from attendance_info r where to_days(r.create_date) = to_days(now()) and r.shop_name = :storeName and r.user_tel = :tel";
        List<AttendanceInfo> records = this.entityManager.createNativeQuery(sql, AttendanceInfo.class).setParameter("tel", tel).setParameter("storeName", storeName).getResultList();
        if (CollectionUtils.isEmpty(records)) {
            return null;
        }
        return records;
    }

    // ******************************************************************************
    // ********************************** CRUD END **********************************
    // ******************************************************************************
}
