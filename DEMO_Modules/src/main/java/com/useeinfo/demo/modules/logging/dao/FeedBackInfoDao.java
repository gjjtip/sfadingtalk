package com.useeinfo.demo.modules.logging.dao;

import com.useeinfo.demo.modules.logging.entity.FeedBackInfo;
import com.useeinfo.demo.modules.logging.entity.FeedBackType;
import com.useeinfo.framework.extend.dao.CrudDao;
import com.useeinfo.framework.sugar.data.QueryParam;
import com.useeinfo.framework.sugar.data.QueryUtils;
import com.useeinfo.framework.sugar.tools.CommonSugar;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chentao on 2017/5/5.
 */
@Repository("feedBackInfoDao")
public class FeedBackInfoDao implements CrudDao<FeedBackInfo> {
    private static final Logger logger = LoggerFactory.getLogger(AttendanceInfoDao.class);

    @PersistenceContext
    private EntityManager entityManager;

    public Map<String, Object> getSearchCondition(Map<String, String> queryHash) {

        Map<String, Object> conditionHash = new LinkedHashMap<>();
        if (queryHash == null) {
            return conditionHash;
        }

        return conditionHash;
    }

    @Override
    public Long totalRecord(Map<String, String> queryHash) {

        Map<String, Object> conditions = getSearchCondition(queryHash);
        TypedQuery<Long> typedQuery = QueryUtils.getTypedQueryByCondition("select count(a) from FeedBackInfo a ", conditions, entityManager, Long.class);
        return typedQuery.getSingleResult();
    }

    @Override
    public List<FeedBackInfo> findList(QueryParam queryParam) {

        String sqlOrder = CommonSugar.getStringDefault(queryParam.getSqlOrder(), "order by a.id desc ");
        String sqlInfo = "select a from FeedBackInfo a " + sqlOrder;

        Map<String, Object> conditions = getSearchCondition(queryParam.getSqlMap());
        TypedQuery<FeedBackInfo> typedQuery = QueryUtils.getTypedQueryByCondition(sqlInfo, conditions, entityManager, FeedBackInfo.class);

        //设定分页信息后返回数据
        return queryParam.findPageList(typedQuery);
    }

    @Override
    public FeedBackInfo findModel(Long id) {
        return entityManager.find(FeedBackInfo.class, id);
    }

    @Override
    public Integer add(FeedBackInfo model) {
        model.setCreateDate(new Date());
        model.setFeedBackTime(new Date());
        entityManager.persist(model);
        logger.info("ProblemBackDaoImpl添加ProblemBack成功！");
        return 1;
    }

    @Override
    public Integer update(FeedBackInfo model) {
        FeedBackInfo existProblemBack = entityManager.find(FeedBackInfo.class, model.getId());
        existProblemBack.setDescribe(model.getDescribe());
        existProblemBack.setFeedBackTime(new Date());
        existProblemBack.setFeedBackType(model.getFeedBackType());
        existProblemBack.setName(model.getName());
        existProblemBack.setTel(model.getTel());
        existProblemBack.setStoreName(model.getStoreName());
        existProblemBack.setUpdateDate(new Date());
        return 1;
    }

    @Override
    public Integer delete(Long id) {
        FeedBackInfo existProblemBack = entityManager.find(FeedBackInfo.class, id);
        entityManager.remove(existProblemBack);
        return 1;
    }
}
