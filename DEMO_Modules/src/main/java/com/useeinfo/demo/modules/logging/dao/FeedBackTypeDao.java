package com.useeinfo.demo.modules.logging.dao;

import com.useeinfo.demo.modules.logging.entity.FeedBackType;
import com.useeinfo.framework.extend.dao.CrudDao;
import com.useeinfo.framework.sugar.data.QueryParam;
import com.useeinfo.framework.sugar.data.QueryUtils;
import com.useeinfo.framework.sugar.tools.CommonSugar;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chentao on 2017/5/5.
 */
@Repository("feedBackTypeDao")
public class FeedBackTypeDao implements CrudDao<FeedBackType> {
    private static final Logger logger = LoggerFactory.getLogger(AttendanceInfoDao.class);

    @PersistenceContext
    private EntityManager entityManager;

    public Map<String, Object> getSearchCondition(Map<String, String> queryHash) {

        Map<String, Object> conditionHash = new LinkedHashMap<>();
        if (queryHash == null) {
            return conditionHash;
        }

        String problemName = queryHash.get("problemName");
        if (!StringUtils.isEmpty(problemName)) {
            conditionHash.put("problemName = ?{paramIndex} ", problemName);
        }

        return conditionHash;
    }

    @Override
    public Long totalRecord(Map<String, String> queryHash) {

        Map<String, Object> conditions = getSearchCondition(queryHash);
        TypedQuery<Long> typedQuery = QueryUtils.getTypedQueryByCondition("select count(a) from FeedBackType a ", conditions, entityManager, Long.class);
        return typedQuery.getSingleResult();
    }

    @Override
    public List<FeedBackType> findList(QueryParam queryParam) {

        String sqlOrder = CommonSugar.getStringDefault(queryParam.getSqlOrder(), "order by a.id desc ");
        String sqlInfo = "select a from FeedBackType a " + sqlOrder;

        Map<String, Object> conditions = getSearchCondition(queryParam.getSqlMap());
        TypedQuery<FeedBackType> typedQuery = QueryUtils.getTypedQueryByCondition(sqlInfo, conditions, entityManager, FeedBackType.class);

        //设定分页信息后返回数据
        return queryParam.findPageList(typedQuery);
    }

    @Override
    public FeedBackType findModel(Long id) {
        return entityManager.find(FeedBackType.class, id);
    }

    @Override
    public Integer add(FeedBackType model) {
        entityManager.persist(model);
        logger.info("ProblemBackDaoImpl添加ProblemBack成功！");
        return 1;
    }

    @Override
    public Integer update(FeedBackType model) {
        FeedBackType existProblemBack = entityManager.find(FeedBackType.class, model.getId());
        existProblemBack.setProblemName(model.getProblemName());
        return 1;
    }

    @Override
    public Integer delete(Long id) {
        FeedBackType existProblemBack = entityManager.find(FeedBackType.class, id);
        entityManager.remove(existProblemBack);
        return 1;
    }
}
