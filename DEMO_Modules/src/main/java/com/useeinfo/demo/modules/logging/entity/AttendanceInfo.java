package com.useeinfo.demo.modules.logging.entity;

import com.useeinfo.framework.extend.entity.DataEntity;
import com.useeinfo.framework.sugar.tools.CommonSugar;
import com.useeinfo.framework.sugar.tools.DateTimeUtils;
import com.useeinfo.framework.sugar.tools.UrlHelp;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.persistence.*;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Author: 龚俊杰
 */
@Entity
@Table(name = "attendance_info")
@GenericGenerator(name = "ATTENDANCE_INFO_GENERATOR", strategy = "enhanced-table",
        parameters = {
                @org.hibernate.annotations.Parameter(name = "table_name", value = "table_generator"),
                @org.hibernate.annotations.Parameter(name = "segment_column_name", value = "segment_name"),
                @org.hibernate.annotations.Parameter(name = "segment_value", value = "attendance_info_id"),
                @org.hibernate.annotations.Parameter(name = "value_column_name", value = "next"),
                @org.hibernate.annotations.Parameter(name = "initial_value", value = "1000"),
                @org.hibernate.annotations.Parameter(name = "increment_size", value = "10"),
                @org.hibernate.annotations.Parameter(name = "optimizer", value = "pooled-lo")
        }
)
//这句注解的意思在编译.java文件的时候，如果变量没有用到，会有提示警告，用@SuppressWarnings("unused")之后 ，警告消失。
@SuppressWarnings("unused")
public class AttendanceInfo extends DataEntity<AttendanceInfo> {

    private Long id;

    private String userName;

    private String userTel;

    private String shopName;

    private Date dutyDate;

    private Long dutyLastTime;

    private Date inDutyTime;

    private String inDutyPhoto;

    private String inLongitude;

    private String inLatitude;

    private Date outDutyTime;

    private String outDutyPhoto;

    private String outLongitude;

    private String outLatitude;
    
    private Date createDate;

    private Date updateDate;

    private Long inExceptionId;

    private Long outExceptionId;

    /**
     * 已经删除Y
     * 未删除N
     */
    private String fakeDelete = "N";

    @Override
    public JSONObject toJSONObject() {
        JSONObject jsonObject = new JSONObject();

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        String inDutyPhoto = CommonSugar.getTypedDefault(getInDutyPhoto(), "");
        if (StringUtils.isNotBlank(inDutyPhoto) && !inDutyPhoto.startsWith("http")) {
            inDutyPhoto = String.format("%s/%s", UrlHelp.getUrlBasePath(request), inDutyPhoto);
        }

        String outDutyPhoto = CommonSugar.getTypedDefault(getOutDutyPhoto(), "");
        if (StringUtils.isNotBlank(outDutyPhoto) && !outDutyPhoto.startsWith("http")) {
            outDutyPhoto = String.format("%s/%s", UrlHelp.getUrlBasePath(request), outDutyPhoto);
        }

        dutyLastTime = CommonSugar.getTypedDefault(dutyLastTime, 0L);
        dutyLastTime = dutyLastTime / 1000 / 60;
        String dateDiff = dutyLastTime / 60 + "小时" + dutyLastTime % 60 + "分";

        jsonObject.put("shopName", CommonSugar.getTypedDefault(getShopName(), ""));
        jsonObject.put("id", CommonSugar.getTypedDefault(getId(), 0L));
        jsonObject.put("userName", CommonSugar.getTypedDefault(getUserName(), ""));
        jsonObject.put("userTel", CommonSugar.getTypedDefault(getUserTel(), ""));
        jsonObject.put("dutyDate", CommonSugar.getTypedDefault(DateTimeUtils.formatDateToStringWithOnlyDate(getDutyDate()), ""));
        jsonObject.put("inDutyTime", CommonSugar.getTypedDefault(DateTimeUtils.format(getInDutyTime(), "yyyy-MM-dd HH:mm"), ""));
        jsonObject.put("inDutyPhoto", CommonSugar.getTypedDefault(inDutyPhoto, ""));
        jsonObject.put("outDutyTime", CommonSugar.getTypedDefault(DateTimeUtils.format(getOutDutyTime(), "yyyy-MM-dd HH:mm"), ""));
        jsonObject.put("outDutyPhoto", CommonSugar.getTypedDefault(outDutyPhoto, ""));
        jsonObject.put("createDate", CommonSugar.getTypedDefault(DateTimeUtils.formatDateToStringWithTime(getCreateDate()), ""));
        jsonObject.put("updateDate", CommonSugar.getTypedDefault(DateTimeUtils.formatDateToStringWithTime(getUpdateDate()), ""));

        return jsonObject;
    }


    @Id
    @GeneratedValue(generator = "ATTENDANCE_INFO_GENERATOR")
    @Column(name = "attendance_info_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "user_name")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Column(name = "user_tel")
    public String getUserTel() {
        return userTel;
    }

    public void setUserTel(String userTel) {
        this.userTel = userTel;
    }

    @Column(name = "shop_name")
    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    @Column(name = "duty_date")
    public Date getDutyDate() {
        return dutyDate;
    }

    public void setDutyDate(Date dutyDate) {
        this.dutyDate = dutyDate;
    }

    @Column(name = "duty_last_time")
    public Long getDutyLastTime() {
        return dutyLastTime;
    }

    public void setDutyLastTime(Long dutyLastTime) {
        if (dutyLastTime != null && dutyLastTime == -1L) {
            dutyLastTime = DateTimeUtils.getDiffSecondsTime(getInDutyTime(), getOutDutyTime());
            if (dutyLastTime < 0) {
                dutyLastTime = dutyLastTime * (-1);
            }
        }
        this.dutyLastTime = dutyLastTime;
    }

    @Column(name = "in_duty_time")
    public Date getInDutyTime() {
        return inDutyTime;
    }

    public void setInDutyTime(Date inDutyTime) {
        this.inDutyTime = inDutyTime;
    }

    @Column(name = "in_duty_photo")
    public String getInDutyPhoto() {
        return inDutyPhoto;
    }

    public void setInDutyPhoto(String inDutyPhoto) {
        this.inDutyPhoto = inDutyPhoto;
    }

    @Column(name = "in_longitude")
    public String getInLongitude() {
        return inLongitude;
    }

    public void setInLongitude(String inLongitude) {
        this.inLongitude = inLongitude;
    }

    @Column(name = "in_latitude")
    public String getInLatitude() {
        return inLatitude;
    }

    public void setInLatitude(String inLatitude) {
        this.inLatitude = inLatitude;
    }

    @Column(name = "out_duty_time")
    public Date getOutDutyTime() {
        return outDutyTime;
    }

    public void setOutDutyTime(Date outDutyTime) {
        this.outDutyTime = outDutyTime;
    }

    @Column(name = "out_duty_photo")
    public String getOutDutyPhoto() {
        return outDutyPhoto;
    }

    public void setOutDutyPhoto(String outDutyPhoto) {
        this.outDutyPhoto = outDutyPhoto;
    }

    @Column(name = "out_longitude")
    public String getOutLongitude() {
        return outLongitude;
    }

    public void setOutLongitude(String outLongitude) {
        this.outLongitude = outLongitude;
    }

    @Column(name = "out_latitude")
    public String getOutLatitude() {
        return outLatitude;
    }

    public void setOutLatitude(String outLatitude) {
        this.outLatitude = outLatitude;
    }

    @Column(name = "create_date")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Column(name = "update_date")
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Column(name = "fake_delete")
    public String getFakeDelete() {
        return fakeDelete;
    }

    public void setFakeDelete(String fakeDelete) {
        this.fakeDelete = fakeDelete;
    }

    @Column(name = "in_exception_id")
    public Long getInExceptionId() {
        return inExceptionId;
    }

    public void setInExceptionId(Long inExceptionId) {
        this.inExceptionId = inExceptionId;
    }

    @Column(name = "out_exception_id")
    public Long getOutExceptionId() {
        return outExceptionId;
    }

    public void setOutExceptionId(Long outExceptionId) {
        this.outExceptionId = outExceptionId;
    }
}
