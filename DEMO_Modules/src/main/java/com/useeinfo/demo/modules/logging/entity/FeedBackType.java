package com.useeinfo.demo.modules.logging.entity;

import com.useeinfo.framework.extend.entity.DataEntity;
import com.useeinfo.framework.sugar.tools.CommonSugar;
import net.sf.json.JSONObject;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by chentao on 2017/5/5.
 */
@Entity
@Table(name = "feed_back_type")
@GenericGenerator(name = "FEED_BACK_TYPE_GENERATOR", strategy = "enhanced-table",
        parameters = {
                @org.hibernate.annotations.Parameter(name = "table_name", value = "table_generator"),
                @org.hibernate.annotations.Parameter(name = "segment_column_name", value = "segment_name"),
                @org.hibernate.annotations.Parameter(name = "segment_value", value = "feed_back_type_id"),
                @org.hibernate.annotations.Parameter(name = "value_column_name", value = "next"),
                @org.hibernate.annotations.Parameter(name = "initial_value", value = "1000"),
                @org.hibernate.annotations.Parameter(name = "increment_size", value = "10"),
                @org.hibernate.annotations.Parameter(name = "optimizer", value = "pooled-lo")
        }
)
public class FeedBackType extends DataEntity<FeedBackType> {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 问题反馈
     */
    private String problemName;

    @Override
    public JSONObject toJSONObject() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("problemName", CommonSugar.getTypedDefault(getProblemName(), ""));
        jsonObject.put("id", CommonSugar.getTypedDefault(getId(), 0L));
        return jsonObject;
    }

    @Id
    @GeneratedValue(generator = "FEED_BACK_TYPE_GENERATOR")
    @Column(name = "feed_back_type_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "problem_name")
    public String getProblemName() {
        return problemName;
    }

    public void setProblemName(String problemName) {
        this.problemName = problemName;
    }
}
