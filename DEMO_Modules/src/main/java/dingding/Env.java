package dingding;


import com.useeinfo.framework.sugar.tools.Configuration;

public class Env {

    public static final String OAPI_HOST = "https://oapi.dingtalk.com";
    public static final String OA_BACKGROUND_URL = "";
    public static final String CORP_ID = Configuration.getConfigurationByName("CORP_ID");
    public static final String CORP_SECRET = Configuration.getConfigurationByName("CORP_SECRET");
    public static final String SSO_Secret = Configuration.getConfigurationByName("SSO_Secret");


    public static String suiteTicket;
    public static String authCode;
    public static String suiteToken;

    public static final String CREATE_SUITE_KEY = "suite4xxxxxxxxxxxxxxx";
    public static final String SUITE_KEY = "";
    public static final String SUITE_SECRET = "";
    public static final String TOKEN = "";
    public static final String ENCODING_AES_KEY = "";

    public static final String AGENT_ID = Configuration.getConfigurationByName("AGENT_ID");
    public static String ACCESS_TOKEN;
    public static String JSAPI_TICKET;
    public static final String HOME_PAGE_URL = Configuration.getConfigurationByName("HOME_PAGE_URL");
    public static final String INTO_STORE_URL = Configuration.getConfigurationByName("INTO_STORE_URL");
    public static final String OUT_STORE_URL = Configuration.getConfigurationByName("OUT_STORE_URL");
    public static final String NONCESTR = "nonceStr";

}
