package dingding.microapp;

import com.useeinfo.framework.sugar.tools.StringConverters;
import dingding.Env;
import dingding.OApiException;
import dingding.OApiResultException;
import dingding.auth.AuthHelper;
import dingding.utils.HttpHelper;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by chjoy on 17/4/17.
 */
public class MicroappHelper {


    private final static Logger logger = LoggerFactory.getLogger(MicroappHelper.class);


    /**
     * access_token(access_token)
     * appIcon(微应用的图标。需要调用上传接口将图标上传到钉钉服务器后获取到的mediaId)
     * appName(微应用的名称。长度限制为1~10个字符)
     * appDesc(微应用的描述。长度限制为1~20个字符)
     * homepageUrl(微应用的移动端主页，必须以http开头或https开头)
     * pcHomepageUrl(非必填,微应用的PC端主页，必须以http开头或https开头，如果不为空则必须与homepageUrl的域名一致)
     * ompLink(非必填,微应用的OA后台管理主页，必须以http开头或https开头)
     */
    public static long createMicroapp(String accessToken,
                                      String appIcon,
                                      String appName,
                                      String appDesc,
                                      String homepageUrl,
                                      String pcHomepageUrl,
                                      String ompLink) throws OApiException {
        String url = Env.OAPI_HOST + "/microapp/create?" +
                "access_token=" + accessToken;
        JSONObject args = new JSONObject();
        args.put("appIcon", appIcon);
        args.put("appName", appName);
        args.put("appDesc", appDesc);
        args.put("homepageUrl", homepageUrl);
        args.put("pcHomepageUrl", pcHomepageUrl);
        args.put("ompLink", ompLink);
        JSONObject response = HttpHelper.httpPost(url, args);
        try {
            if (response != null) {
                logger.info(String.format("操作失败 errcode:%s errmsg:%s", response.getString("errcode"), response.getString("errmsg")));
                return StringConverters.ToLong(response.getString("id"));
            }
            return 0L;
        } catch (Exception e) {
            return 0L;
        }
    }

    public static void main(String[] args) {
        try {
            String accessToken = AuthHelper.getAccessToken();
            System.out.println("首先获取accesstoken:" + accessToken);
            String url = Env.OAPI_HOST + "/user/get_admin?" +
                    "access_token=" + accessToken;
            JSONObject response = HttpHelper.httpGet(url);
//			manager9268(管理员userId)
            System.out.println(response.toString());


//

//			File file = new File("/Users/chjoy/javaSoft/1.jpg");
//			MediaHelper.MediaUploadResult mediaUploadResult =  MediaHelper.upload(accessToken,MediaHelper.TYPE_IMAGE,file);
//			System.out.println("获取微应用的图标appIcon:"+ mediaUploadResult.media_id);
//			MediaHelper.download(accessToken,mediaUploadResult.media_id,"/Users/chjoy/javaSoft/2.jpg");
////			long  id = MicroappHelper.createMicroapp(accessToken,
////					mediaUploadResult.media_id,
////					"梨春之光标题",
////					"梨春之光描述",
////					"http://icheckusee.chenlichun.me/","","");
////			System.out.println(id);
        } catch (Exception e) {
            System.out.println(e);
        }


    }

}
