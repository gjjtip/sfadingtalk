package com.useeinfo.demo.open.dashboard;

import antlr.StringUtils;
import com.useeinfo.demo.common.config.ConstantKeyFilePath;
import com.useeinfo.demo.modules.logging.biz.AttendanceInfoBiz;
import com.useeinfo.demo.modules.logging.entity.AttendanceInfo;
import com.useeinfo.demo.modules.logging.entity.FeedBackInfo;
import com.useeinfo.framework.extend.action.BaseAction;
import com.useeinfo.framework.sugar.data.QueryParam;
import com.useeinfo.framework.sugar.func.file.FileUtils;
import com.useeinfo.framework.sugar.tools.DateTimeUtils;
import com.useeinfo.framework.sugar.tools.StringConverters;
import dingding.Env;
import dingding.OApiException;
import dingding.auth.AuthHelper;
import dingding.utils.WeChatSign;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by gjj
 */
@Controller
@RequestMapping("/web/admin/attendance")
public class AttendanceInfoAction extends BaseAction {

    @Autowired
    private AttendanceInfoBiz attendanceInfoBiz;

    /**
     * 打开列表页面
     */
    @RequestMapping("/getAttendanceInfoListPage")
    public ModelAndView getAttendanceInfoListPage() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("pages/attendance/attendanceList");
        return modelAndView;
    }

    /**
     * 分页获取JSON数据
     */
    @RequestMapping("/getAttendanceInfoListJSON")
    @ResponseBody
    public JSONObject getPositionListJSON(@RequestParam(value = "page", required = false) String pageNowParam,
                                          @RequestParam(value = "rows", required = false) String pageSizeParam,
                                          @RequestParam(value = "userTel", required = false) String userTel) {
        QueryParam queryParam = new QueryParam(pageNowParam, pageSizeParam);
        queryParam.getSqlMap().put("userTel", userTel);
        JSONObject jsonObject = attendanceInfoBiz.findJSONList(queryParam);
        return attendanceInfoBiz.findJSONList(queryParam);
    }


    /**
     * 获取编辑页面
     */
//    @RequestMapping("/getAttendanceInfoEditPage")
//    public ModelAndView getPositionEditPage(@RequestParam(value = "attendanceInfoId", required = false) String attendanceInfoIdParam) {
//
//        Long attendanceInfoId = StringConverters.ToLong(attendanceInfoIdParam);
//
//        AttendanceInfo attendanceInfo = null;
//        if (attendanceInfoId != null) {
//            attendanceInfo = attendanceInfoBiz.findModel(attendanceInfoId);
//        }
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("pages/attendance/attendanceEdit");
//        modelAndView.addObject("attendanceInfo", attendanceInfo);
//        return modelAndView;
//    }

//    /**
//     * 执行提交的修改请求
//     */
//    @RequestMapping(value = "/executeAttendanceInfoEdit", produces = {"text/html;charset=UTF-8"})
//    @ResponseBody
//    public String executeattendanceInfoEdit(AttendanceInfo attendanceInfo) {
//        //判断特殊职位名称是否为空
//        if (StringUtils.isBlank(attendanceInfo.getProblemName())) {
//            return "请填写反馈类型";
//        }
//
//        QueryParam queryParam = new QueryParam();
//        queryParam.getSqlMap().put("attendanceInfoName", attendanceInfo.getProblemName());
//        List<AttendanceInfo> attendanceInfos = attendanceInfoBiz.findList(queryParam);
//        if(attendanceInfos.size()>0){
//            return "反馈类型重复，请重新填写！";
//        }
//        //新增或修改
//        attendanceInfoBiz.addOrUpdate(attendanceInfo);
//        return "success";
//    }


    /**
     * 真删除机构用户信息
     */
    @RequestMapping("/logicRemoveAttendanceInfo")
    @ResponseBody
    public String logicRemoveAttendanceInfo(@RequestParam(value = "id", required = false) String attendanceInfoIdParam) {

        Long attendanceInfoId = StringConverters.ToLong(attendanceInfoIdParam);
        attendanceInfoBiz.delete(attendanceInfoId);
        return "1";
    }


//    /**
//     * 签到
//     */
//    @RequestMapping("/reportAttendanceInfo")
//    @ResponseBody
//    public String reportAttendanceInfo(@RequestParam(value = "name", required = false) String name,
//                                       @RequestParam(value = "storeName", required = false) String storeName,
//                                       @RequestParam(value = "tel", required = false) String tel,
//                                       @RequestParam(value = "latitude", required = false) String latitude,
//                                       @RequestParam(value = "longitude", required = false) String longitude,
//                                       @RequestParam(value = "commute", required = false) String commute) {
//        String dutyTime = DateTimeUtils.format(new Date(), "yyyyMMdd");
//
//
//        return "1";
//    }
}
