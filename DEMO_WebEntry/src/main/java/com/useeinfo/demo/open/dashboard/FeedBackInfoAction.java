package com.useeinfo.demo.open.dashboard;

import com.useeinfo.demo.modules.logging.biz.FeedBackInfoBiz;
import com.useeinfo.demo.modules.logging.entity.FeedBackInfo;
import com.useeinfo.framework.extend.action.BaseAction;
import com.useeinfo.framework.sugar.tools.StringConverters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * Created by chentao on 2017/5/3.
 */
@Controller
@RequestMapping("/web/admin/feedBackInfo")
public class FeedBackInfoAction extends BaseAction {

    @Autowired
    private FeedBackInfoBiz feedBackInfoBiz;


    @RequestMapping("/addOrUpdateFeedBackInfo")
    @ResponseBody
    public ModelAndView addOrUpdateFeedBackInfo(@RequestParam(value = "name", required = false) String name,
                                                @RequestParam(value = "tel", required = false) String tel,
                                                @RequestParam(value = "feedBackType", required = false) String feedBackType,
                                                @RequestParam(value = "describe ", required = false) String describe,
                                                @RequestParam(value = "storeName ", required = false) String storeName) {
        FeedBackInfo feedBackInfo = new FeedBackInfo();
        feedBackInfo.setTel(tel);
        feedBackInfo.setName(name);
        feedBackInfo.setFeedBackType(feedBackType);
        feedBackInfo.setDescribe(describe);
        feedBackInfo.setStoreName(storeName);
        feedBackInfo.setFeedBackTime(new Date());
        feedBackInfo.setCreateDate(new Date());
        feedBackInfoBiz.addOrUpdate(feedBackInfo);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("pages/feedBackType/feedBackTypeEdit");
        modelAndView.addObject("feedBackInfo", feedBackInfo);
        return modelAndView;
    }
}
