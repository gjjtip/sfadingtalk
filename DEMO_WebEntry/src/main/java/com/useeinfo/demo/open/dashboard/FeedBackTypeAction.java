package com.useeinfo.demo.open.dashboard;

import com.useeinfo.demo.modules.logging.biz.FeedBackTypeBiz;
import com.useeinfo.demo.modules.logging.entity.FeedBackType;
import com.useeinfo.framework.extend.action.BaseAction;
import com.useeinfo.framework.sugar.data.QueryParam;
import com.useeinfo.framework.sugar.tools.StringConverters;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by chentao on 2017/5/3.
 */
@Controller
@RequestMapping("/web/admin/feedBackType")
public class FeedBackTypeAction extends BaseAction {

    @Autowired
    private FeedBackTypeBiz feedBackTypeBiz;

    /**
     * 打开列表页面
     */
    @RequestMapping("/getFeedBackTypeListPage")
    public ModelAndView getFeedBackTypeListPage() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("pages/feedBackType/feedBackTypeList");
        return modelAndView;
    }

    /**
     * 分页获取JSON数据
     */
    @RequestMapping("/getFeedBackTypeListJSON")
    @ResponseBody
    public JSONObject getFeedBackTypeListJSON(@RequestParam(value = "page", required = false) String pageNowParam,
                                              @RequestParam(value = "rows", required = false) String pageSizeParam,
                                              @RequestParam(value = "problemName", required = false) String problemName) {
        QueryParam queryParam = new QueryParam(pageNowParam, pageSizeParam);
        queryParam.getSqlMap().put("problemName", problemName);
        return feedBackTypeBiz.findJSONList(queryParam);
    }


    /**
     * 获取编辑页面
     */
    @RequestMapping("/getFeedBackTypeEditPage")
    public ModelAndView getFeedBackTypeEditPage(@RequestParam(value = "feedBackTypeId", required = false) String feedBackTypeIdParam) {

        Long feedBackTypeId = StringConverters.ToLong(feedBackTypeIdParam);

        FeedBackType feedBackType = null;
        if (feedBackTypeId != null) {
            feedBackType = feedBackTypeBiz.findModel(feedBackTypeId);
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("pages/feedBackType/feedBackTypeEdit");
        modelAndView.addObject("feedBackType", feedBackType);
        return modelAndView;
    }


    /**
     * 执行提交的修改请求
     */
    @RequestMapping(value = "/executeFeedBackTypeEdit", produces = {"text/html;charset=UTF-8"})
    @ResponseBody
    public String executeFeedBackTypeEdit(FeedBackType feedBackType) {
        //判断是否重复
        QueryParam queryParam = new QueryParam();
        queryParam.getSqlMap().put("problemName", feedBackType.getProblemName());
        List<FeedBackType> feedBackTypes = feedBackTypeBiz.findList(queryParam);
        if (feedBackTypes.size() > 0) {
            return "反馈类型重复，请重新填写！";
        }
        //新增或修改
        feedBackTypeBiz.addOrUpdate(feedBackType);
        return "success";
    }


    /**
     * 真删除机构用户信息
     */
    @RequestMapping("/logicRemoveFeedBackType")
    @ResponseBody
    public String logicRemoveFeedBackType(@RequestParam(value = "id", required = false) String feedBackTypeIdParam) {

        Long feedBackTypeId = StringConverters.ToLong(feedBackTypeIdParam);
        feedBackTypeBiz.delete(feedBackTypeId);
        return "1";
    }

    /**
     * 获取问题反馈类型下拉框
     */
    @RequestMapping("/getFeedbackType")
    @ResponseBody
    public JSONObject getFeedbackType() {
        QueryParam queryParam = new QueryParam();
        List<FeedBackType> feedBackTypeList = feedBackTypeBiz.findList(queryParam);
        JSONArray jsArr = new JSONArray();
        JSONObject returnJson = new JSONObject();
        for (FeedBackType feedBackType : feedBackTypeList) {
            JSONObject json = feedBackType.toJSONObject();
            jsArr.add(json);
        }
        returnJson.put("jsArr", jsArr);
        return returnJson;
    }

    @RequestMapping("/getPage")
    public ModelAndView getPage() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("mobile/sign");
        return modelAndView;
    }
}
