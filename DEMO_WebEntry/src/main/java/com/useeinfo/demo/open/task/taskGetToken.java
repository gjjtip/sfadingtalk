package com.useeinfo.demo.open.task;

import dingding.Env;
import dingding.OApiException;
import dingding.auth.AuthHelper;
import org.springframework.stereotype.Component;

/**
 * Created by 龚俊杰
 * 定时任务每隔1小时50分钟就获取一次AccessToken
 */

@Component("task")
public class taskGetToken {
    public void getToken() throws OApiException {
        Env.ACCESS_TOKEN = AuthHelper.getAccessToken();
        System.out.println("access token刷新为:" + Env.ACCESS_TOKEN);

        Env.JSAPI_TICKET = AuthHelper.getJsapiTicket(Env.ACCESS_TOKEN);
        System.out.println("jsapi ticket刷新为:" + Env.JSAPI_TICKET);
    }
}
