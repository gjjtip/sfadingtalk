<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>问题反馈</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <link href="<c:url value="/resources/lib/bootstrap-3.3.1-dist/css/bootstrap.min.css" />" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<c:url value="/resources/lib/jquery/jquery-1.11.3.min.js" />"></script>
    <script src="<c:url value="/resources/lib/bootstrap-3.3.1-dist/js/bootstrap.min.js" />"></script>
    <script src="<c:url value="/resources/js/urlConfig.js" />"></script>
</head>
<body>


<div style="display:none;"><input type="text" id="dutyType" name="dutyType" value="${dutyType}"></div>
<div style="display:none;"><input type="text" id="userName" name="userName" value="${userName}"></div>
<div style="display:none;"><input type="text" id="userTel" name="userTel" value="${userTel}"></div>
<div class="form-group">
    <label for="storeName">门店名称:</label>
    <input type="text" class="form-control" id="storeName" name="storeName" placeholder="" value="${storeName}">
</div>

<div class="form-group">
    <label for="feedbackType">反馈类型:</label>
    <select id="feedbackType" name="feedbackType" class="form-control" value="${type}">
        <option value="">--请选择--</option>
        <c:forEach items="${feedBackTypeList}" var="checkStatus">
            <option value="${checkStatus.problemName}"
                    <c:if test="${checkStatus==feedBackType.problemName}">selected="selected"</c:if>>${checkStatus.getProblemName()}</option>
        </c:forEach>
    </select>
</div>
<div class="form-group">
    <label for="describe">描述:</label>
    <input type="textArea" class="form-control" id="describe" name="describe" placeholder="" value="${describe}">
</div>
<button type="button" id="submitFeedback" class="btn btn-primary btn-lg btn-block">提交</button>

</body>
<script>
    document.querySelector('#submitFeedback').onclick = function () {

        $.ajax({
            url: urlConfig + "/access/attend/feedback.action",
            data: {
                dutyType: $("#dutyType").val(),
                feedbackType: $("#feedbackType").val(),
                userName: $("#userName").val(),
                userTel: $("#userTel").val(),
                describe: $("#describe").val(),
                storeName: $("#storeName").val()
            },
            type: 'POST',
            success: function (data) {
                if (data == "success") {
                    alert("问题反馈成功");
                    window.location = urlConfig + "/access/home.action";
                } else {
                    alert(data);
                }
            },
            error: function (xhr, errorType, error) {

            }
        });
    };
</script>
<script>
    $("#feedbackType").prop("value", "${type}");
</script>
</html>
