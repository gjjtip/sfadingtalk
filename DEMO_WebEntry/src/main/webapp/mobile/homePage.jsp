<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>签到列表</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <link href="<c:url value="/resources/lib/bootstrap-3.3.1-dist/css/bootstrap.min.css" />" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<c:url value="/resources/lib/jquery/jquery-1.11.3.min.js" />"></script>
    <script src="<c:url value="/resources/lib/bootstrap-3.3.1-dist/js/bootstrap.min.js" />"></script>

    <link href="<c:url value="/resources/css/style2.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/moment.min.js" />"></script>
    <script src="<c:url value="/resources/js/urlConfig.js" />"></script>
</head>
<body>
<div id="clock" class="light">
    <div class="display">
        <div class="date"></div>
        <div class="digits"></div>
    </div>
</div>
<input class="form-control" id="userName" name="userName" value="" readonly="readonly">
<button type="button" id="intoStore" onclick="intoStore()">签到</button>

<div id="attendanceInfoList" style="margin-top:20px">
    <table id="attendance" border="1px solid" width="100%">
    </table>
</div>

</body>
<script>
    $(function () {
        var clock = $('#clock');
        //定义数字数组0-9
        var digit_to_name = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
        //定义星期
        var weekday = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];
        var digits = {};
        //定义时分秒位置
        var positions = [
            'h1', 'h2', ':', 'm1', 'm2', ':', 's1', 's2'
        ];
        //构建数字时钟的时分秒
        var digit_holder = clock.find('.digits');
        $.each(positions, function () {
            if (this == ':') {
                digit_holder.append('<div class="dots">');
            }
            else {
                var pos = $('<div>');
                for (var i = 1; i < 8; i++) {
                    pos.append('<span class="d' + i + '">');
                }
                digits[this] = pos;
                digit_holder.append(pos);
            }
        });
        // 让时钟跑起来
        (function update_time() {
            //调用moment.js来格式化时间
            var now = moment().format("HHmmss");
            digits.h1.attr('class', digit_to_name[now[0]]);
            digits.h2.attr('class', digit_to_name[now[1]]);
            digits.m1.attr('class', digit_to_name[now[2]]);
            digits.m2.attr('class', digit_to_name[now[3]]);
            digits.s1.attr('class', digit_to_name[now[4]]);
            digits.s2.attr('class', digit_to_name[now[5]]);
            var date = moment().format("YYYY年MM月DD日");
            var week = weekday[moment().format('d')];
            $(".date").html(date + ' ' + week);
            // 每秒钟运行一次
            setTimeout(update_time, 1000);
        })();
//        getAttendanceInfoList();
    });
</script>
<script>
</script>
<script src="http://g.alicdn.com/dingding/open-develop/1.1.9/dingtalk.js"></script>
<script>
    dd.config({
        agentId: '${agentId}', // 必填，微应用ID
        corpId: '${corpId}',//必填，企业ID
        timeStamp:${timeStamp}, // 必填，生成签名的时间戳
        nonceStr: '${nonceStr}', // 必填，生成签名的随机串
        signature: '${signature}', // 必填，签名
        type: 0,   //选填。0表示微应用的jsapi,1表示服务窗的jsapi。不填默认为0。该参数从dingtalk.js的0.8.3版本开始支持
        jsApiList: ['runtime.permission.requestAuthCode'] // 必填，需要使用的jsapi列表，注意：不要带dd。
    });
</script>
<script type="text/javascript">
    dd.ready(function () {
        dd.runtime.permission.requestAuthCode({
            corpId: '${corpId}',
            onSuccess: function (info) {
                var code = info.code;
                $.ajax({
                    url: urlConfig + "/access/getList.action",
                    data: {
                        accessToken: '${accessToken}',
                        code: code
                    }
                    ,
                    type: 'GET',
                    success: function (data, status, xhr) {
//                        var info = JSON.parse(data);
                        $("#attendance").html("");
                        var jsonArray = data.jsArray;
                        $("#userName").attr("value", "尊敬的" + data.userInfo.name + " 您今天已经到访了" + jsonArray.length + "家门店");
                        for (var i = 0; i < jsonArray.length; i++) {
                            var shopName = jsonArray[i].shopName;
                            var inLatitude = jsonArray[i].inLatitude;
                            var inLongitude = jsonArray[i].inLongitude;
                            var inDutyTime = jsonArray[i].inDutyTime;
                            var outDutyTime = jsonArray[i].outDutyTime;
                            var inExceptionId = jsonArray[i].inExceptionId;
                            var outExceptionId = jsonArray[i].outExceptionId;
                            $("#attendance").append('<tr style="height: 60px;"><td rowspan="2" align="center" style="width: 40%;">' + shopName +
                                    '</td><td style="width: 60%"><div style="float:left">' + inDutyTime + "</div>" +
                                    '<div style="float:left;margin-left:20px"><img style="height: 30px;width:40px" src="<c:url value='/resources/images/photo.png' />"</div>' +
                                    '<div style="float:left;margin-left:60px"><img onclick="viewMap(' + inLatitude + ',' + inLongitude + ',' + shopName + ')" style="height: 30px;width:40px" src="<c:url value='/resources/images/map.png' />"</div></td></tr>');

                            if (outExceptionId == null) {
                                if (outDutyTime == null) {
                                    $("#attendance").append('<tr style="height: 60px;"><td style="width: 60%"><button type="button" id="outStore" onclick="outStore()">离店签出</button></td></tr>');
                                } else {
                                    $("#attendance").append('<tr style="height: 60px;"><td style="width: 60%"><div style="float:left">' + outDutyTime + '</div>' +
                                            '<div style="float:left;margin-left:20px"><img style="height: 30px;width:40px" src="<c:url value='/resources/images/photo.png' />"</div>' +
                                            '<div style="float:left;margin-left:60px"><img style="height: 30px;width:40px" src="<c:url value="/resources/images/map.png" />"</div></td></tr>');
                                }
                            } else {
                                $("#attendance").append('<tr style="height: 60px;"><td style="width: 60%"><a onclick=exceptionInfo(' + outExceptionId + ')>异常</a></td></tr>');
                            }
                        }
                    }

                    ,
                    error: function (xhr, errorType, error) {
                        logger.e("yinyien:" + '${corpId}');
                        alert(errorType + ', ' + error);
                    }
                });
            },
            onFail: function (err) {
                alert('fail: ' + JSON.stringify(err));
            }
        })
        ;

    })
    ;
    dd.error(function (err) {
        alert('dd error: ' + JSON.stringify(err));
    });
    function viewMap(lat, lon, shopName) {
        dd.biz.map.view({
//            latitude: 39.903578, // 纬度
//            longitude: 116.473565, // 经度
//            title: "北京国家广告产业园" // 地址/POI名称
            latitude: lat, // 纬度
            longitude: lon, // 经度
            title: shopName // 地址/POI名称
        });
    }
    function intoStore() {
        window.location = urlConfig + "/access/in.action";

    }
    function outStore() {
        window.location = urlConfig + "/access/out.action";
    }
    function exceptionInfo(outExceptionId) {
        window.location = urlConfig + "/access/feedback/query.action?exceptionId=" + outExceptionId;
    }
</script>
</html>
