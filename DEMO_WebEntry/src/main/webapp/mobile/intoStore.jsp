<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>进店签到</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no, width=device-width">
    <link rel="stylesheet" href="http://cache.amap.com/lbs/static/main1119.css"/>
    <script src="http://cache.amap.com/lbs/static/es5.min.js"></script>
    <script src="http://webapi.amap.com/maps?v=1.3&key=cdb1b0dc5681eb2de7e78928ad2cfc60"></script>
    <script type="text/javascript" src="http://cache.amap.com/lbs/static/addToolbar.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <link href="<c:url value="/resources/lib/bootstrap-3.3.1-dist/css/bootstrap.min.css" />" rel="stylesheet">
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>-->
    <!--<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>-->
    <![endif]-->
    <script src="<c:url value="/resources/lib/jquery/jquery-1.11.3.min.js" />"></script>
    <script src="<c:url value="/resources/lib/bootstrap-3.3.1-dist/js/bootstrap.min.js" />"></script>

    <link href="<c:url value="/resources/css/style2.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/moment.min.js" />"></script>
    <script src="<c:url value="/resources/js/urlConfig.js" />"></script>
</head>
<body>
<div>

    <div id="clock" class="light">
        <div class="display">
            <div class="date"></div>
            <div class="digits"></div>
        </div>
    </div>


    <div id="container1"
         style="width:445px;height:250px;border-color: #0070a9;border:1px solid #b3b3b3;margin-top: 20px"></div>
    <div class="form-group" style="margin-top: 20px">
        <label for="store" class="btn btn-default btn-block">门店:</label>
        <input type="textArea" class="form-control" id="store" name="store" placeholder="请输入门店名称" value="">
    </div>

    <div class="form-group" style="margin-top: 20px">
        <button type="button" id="chooseImage" class="btn btn-default btn-block">拍照</button>
        <br/>

        <div id="photoDiv" style="display: none">
            <img src="" id="v_photo" style="width:100%;"/>

        </div>

    </div>

    <button type="button" id="submitOnDuty" class="btn btn-primary btn-lg btn-block">签到</button>
    <div style="display:none;"><input type="text" id="photo" name="photo"></div>
    <div style="display:none;"><input type="text" id="longitude" name="longitude"></div>
    <div style="display:none;"><input type="text" id="latitude" name="latitude"></div>
    <div style="display:none;"><input type="text" id="userName" name="userName"></div>
    <div style="display:none;"><input type="text" id="userTel" name="userTel"></div>
    <div style="margin: 20px">
        <a>如无法正常签到，请</a><a id="feedback" style="color: red" onclick="feedback()">反馈问题</a>
    </div>


</div>

</body>
<script>
    $(function () {
        var clock = $('#clock');
        //定义数字数组0-9
        var digit_to_name = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
        //定义星期
        var weekday = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];
        var digits = {};
        //定义时分秒位置
        var positions = [
            'h1', 'h2', ':', 'm1', 'm2', ':', 's1', 's2'
        ];
        //构建数字时钟的时分秒
        var digit_holder = clock.find('.digits');
        $.each(positions, function () {
            if (this == ':') {
                digit_holder.append('<div class="dots">');
            }
            else {
                var pos = $('<div>');

                for (var i = 1; i < 8; i++) {
                    pos.append('<span class="d' + i + '">');
                }
                digits[this] = pos;
                digit_holder.append(pos);
            }
        });
        // 让时钟跑起来
        (function update_time() {
            //调用moment.js来格式化时间
            var now = moment().format("HHmmss");

            digits.h1.attr('class', digit_to_name[now[0]]);
            digits.h2.attr('class', digit_to_name[now[1]]);
            digits.m1.attr('class', digit_to_name[now[2]]);
            digits.m2.attr('class', digit_to_name[now[3]]);
            digits.s1.attr('class', digit_to_name[now[4]]);
            digits.s2.attr('class', digit_to_name[now[5]]);

            var date = moment().format("YYYY年MM月DD日");
            var week = weekday[moment().format('d')];
            $(".date").html(date + ' ' + week);
            // 每秒钟运行一次
            setTimeout(update_time, 1000);
        })();
    });
</script>

<script src="http://g.alicdn.com/dingding/open-develop/1.1.9/dingtalk.js"></script>

<script>

    dd.config({
        agentId: '${agentId}', // 必填，微应用ID
        corpId: '${corpId}',//必填，企业ID
        timeStamp:${timeStamp}, // 必填，生成签名的时间戳
        nonceStr: '${nonceStr}', // 必填，生成签名的随机串
        signature: '${signature}', // 必填，签名
        type: 0,   //选填。0表示微应用的jsapi,1表示服务窗的jsapi。不填默认为0。该参数从dingtalk.js的0.8.3版本开始支持
        jsApiList: ['runtime.info', 'biz.contact.choose', 'runtime.permission.requestAuthCode',
            'device.notification.confirm', 'device.notification.alert', 'device.notification.showPreloader', 'dd.device.notification.hidePreloader',
            'device.notification.prompt', 'biz.ding.post', 'biz.user.get', 'device.geolocation.get', 'biz.map.locate', 'biz.util.uploadImageFromCamera',
            'biz.map.view',
            'biz.util.openLink'] // 必填，需要使用的jsapi列表，注意：不要带dd。
    });
</script>
<script type="text/javascript">
    dd.ready(function () {

        dd.runtime.permission.requestAuthCode({
            corpId: '${corpId}',
            onSuccess: function (info) {
                var code = info.code;
                $.ajax({
                    url: urlConfig + "/access/user.action",
                    data: {
                        accessToken: '${accessToken}',
                        code: code
                    },
                    type: 'GET',
                    success: function (data, status, xhr) {
//                        var info = JSON.parse(data);
                        $("#userName").val(data.name);
                        $("#userTel").val(data.mobile);

                    },
                    error: function (xhr, errorType, error) {
                        logger.e("yinyien:" + '${corpId}');
                        alert(errorType + ', ' + error);
                    }
                });

            },
            onFail: function (err) {
                alert('fail: ' + JSON.stringify(err));
            }
        });

        var longitude;
        var latitude;
        dd.device.geolocation.get({
            targetAccuracy: 50,
            coordinate: 1,
            withReGeocode: Boolean,
            onSuccess: function (result) {
                longitude = result.longitude;
                latitude = result.latitude;
                $("#longitude").val(longitude);
                $("#latitude").val(latitude);
                var map = new AMap.Map("container1", {
                    resizeEnable: true,
                    center: [longitude, latitude],//地图中心点
                    zoom: 13 //地图显示的缩放级别
                });
                //添加点标记，并使用自己的icon
                new AMap.Marker({
                    map: map,
                    position: [longitude, latitude],
                    icon: new AMap.Icon({
                        size: new AMap.Size(40, 50),  //图标大小
                        image: "http://webapi.amap.com/theme/v1.3/images/newpc/way_btn2.png",
                        imageOffset: new AMap.Pixel(0, -60)
                    })
                });

            },
            onFail: function (err) {
            }
        });

    });

    document.querySelector('#chooseImage').onclick = function () {
        dd.biz.util.uploadImageFromCamera({
            compression: true,
            onSuccess: function (re) {
                $("#v_photo").attr("src", re);
                $("#photoDiv").show();
                $("#photo").val(re);
            },
            onFail: function (err) {
            }
        })
    };

    document.querySelector('#submitOnDuty').onclick = function () {
        $.ajax({
            url: urlConfig + "/access/attend/submit.action?dutyType=in",
            data: {
                photo: $("#photo").val(),
                longitude: $("#longitude").val(),
                latitude: $("#latitude").val(),
                userName: $("#userName").val(),
                userTel: $("#userTel").val(),
                store: $("#store").val()
            },
            type: 'POST',
            success: function (result) {
                if (result == "success") {
                    alert("签到成功");
                    window.location = urlConfig + "/access/home.action";
                } else {
                    alert(result);
                }

            },
            error: function (xhr, errorType, error) {

            }
        });
    };

    function feedback() {
        window.location = urlConfig + "/access/feedback.action?dutyType=in&userTel=" + $("#userTel").val() + "&userName=" + $("#userName").val();

    }
</script>
</html>
