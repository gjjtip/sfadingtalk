<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <title>签到数据</title>
    <jsp:include page="/include/common.jsp"/>
    <script type="text/javascript" src="<c:url value="/resources/lib/jquery/plugin/jquery.form.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/lib/My97DatePicker/WdatePicker.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/verify.js" />"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#dgAttendance').datagrid({
                url: '<c:url value="/web/admin/attendance/getAttendanceInfoListJSON.action" />',
                title: '签到数据',
                singleSelect: true,
                fit: true,
                fitColumns: true,
                toolbar: '#tbAttendance',
                rownumbers: true,
                pagination: true,
                pageSize: 20,
                columns: [[
                    {
                        field: 'edit', title: '操作', width: 30, align: 'center', formatter: function (val, row) {
                        if (row.id < 0) {
                            return '';
                        }
                        <%--var editUrl = "<c:url value="/web/admin/attendance/getAttendanceInfoEditPage.action?attendanceInfoId=" />" + row.id;--%>
                        return '&nbsp;' +
//                                '<a href="javascript:void(0)" onclick="openWindow(\'#wAttendance\', \'' + editUrl + '\')">编辑</a>' +
//                                '&nbsp;|&nbsp;' +
                                '<a href="javascript:void(0)" onclick="submitRemove(\'' + row.id + '\', \'1\')">删除</a>' +
                                '&nbsp;';
                    }
                    },
                    {field: 'id', title: '编号', width: 20, align: 'center'},
                    {field: 'userName', title: '姓名', width: 60, align: 'center'},
                    {field: 'userTel', title: '手机号', width: 60, align: 'center'},
                    {field: 'shopName', title: '门店名称', width: 60, align: 'center'},
                    {field: 'dutyDate', title: '日期', width: 60, align: 'center'},
                    {field: 'inDutyTime', title: '进店时间', width: 60, align: 'center'},
                    {field: 'inDutyPhoto', title: '进店照片', width: 60, align: 'center'},
                    {field: 'outDutyTime', title: '离店时间', width: 60, align: 'center'},
                    {field: 'outDutyPhoto', title: '离店照片', width: 60, align: 'center'}
                ]]
            });

            formatPagination("dgAttendance");

            vEasyUIUtil.createWindow("wAttendance", {
                title: '新增/编辑',
                width: 640,
                height: vSugar.getMaxWinHeight("mainPanel", 600),
                modal: true,
                closed: true,
                iconCls: 'icon-save'
            });

            vEasyUIUtil.createWindow("wAdminUser2", {
                width: '49%',
                height: vSugar.getMaxWinHeight("mainPanel", 600),
                modal: true,
                maximized: false,
                closed: true,
                collapsible: false,
                minimizable: false,
                maximizable: true,
                iconCls: 'icon-save'
            });
            $("#wAdminUser2").window("body").css("padding", "0");
        });

        /******************************* 数据操作 *******************************/

        function submitRemove(id) {
            if (!confirm("确认删除")) {
                return;
            }
            // isFakeDelete=1表示假删除
            $.post("<c:url value="/web/admin/attendance/logicRemoveAttendanceInfo.action"/>", {id: id},
                    function (result) {
                        if (result == 1) {
                            $('#dgAttendance').datagrid('reload');
                        }
                    }
            );
        }

        function submitSearch() {
            var dgAttendance = $('#dgAttendance');

            dgAttendance.datagrid('options').pageNumber = 1;
            dgAttendance.datagrid('getPager').pagination("options").pageNumber = 1;
            var param = {
//                attendanceName: $("#attendanceName").val()
            };
            dgAttendance.datagrid({queryParams: param}, 'reload');
        }

    </script>
</head>
<body class="easyui-layout">
<div data-options="region:'center',border:false" class="mainPanel">
    <div id="tbAttendance" class="toolBarPadding">
        <div style="float:left;">
            <sec:authorize access="hasAnyAuthority('导出')">
                <a href="javascript:void(0)" class="easyui-linkbutton v-align-middle" target="_blank" id="exportId"
                   data-options="iconCls:'icon-edit',plain:true"
                   onclick="document.getElementById('printForm').submit();">导出&nbsp;</a>
            </sec:authorize>
        </div>
        <div style="float:right;">
            <div>
                <%--<label for="attendanceName" class="v-align-middle" style="width:160px;">反馈类型：</label>--%>
                <%--<input type="text" class="easyui-textbox" id="attendanceName" name="attendanceName" value=""--%>
                <%--style="width:160px;"/>--%>
                &nbsp;
                <a href="javascript:void(0)" class="easyui-linkbutton v-align-middle"
                   data-options="iconCls:'icon-search'"
                   onclick="submitSearch()">搜索&nbsp;</a>
            </div>
        </div>
        <div style="float:right;">
        </div>
        <div style="clear:both;"></div>
    </div>
    <table id="dgAttendance"></table>
</div>


</body>
</html>