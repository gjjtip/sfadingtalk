<%--
  User: val.jzp
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<form id="myFormFeedBackType" name=""
      action="<c:url value="/web/admin/feedBackType/executeFeedBackTypeEdit.action" />"
      method="post">
    <table class="table-container">
        <tr>
            <td class="item-name">&nbsp;</td>
            <td class="item-value">
                <a href="javascript:void(0)" class="easyui-linkbutton" onclick="return submitEdit()">提交</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" onclick="closeWindow('#wFeedBackType')">取消</a>
            </td>
        </tr>
        <c:if test="${feedBackType != null}">
            <tr>
                <td class="item-name">编号：</td>
                <td class="item-value">
                    <p>${feedBackType.id}</p>
                    <input type="hidden" name="id" value="${feedBackType.id}"/>
                </td>
            </tr>
        </c:if>

        <tr>
            <td class="item-name"><label for="f_feedBackType">反馈类型：</label></td>
            <td class="item-value">
                <input type="text" id="f_feedBackType" name="problemName" value="${feedBackType.problemName}"
                       onKeypress="javascript:if(event.keyCode == 32)event.returnValue = false;"/>
            </td>
        </tr>

    </table>
</form>
<script type="text/javascript">
    function submitEdit() {
        if ($("#f_feedBackType").val().trim() == "") {
            alert("请填写反馈类型");
            return false;
        }
        $("#myFormFeedBackType").ajaxSubmit({
            success: function (html, status) {
                if (html == "success") {
                    $("#wFeedBackType").window('close');
                    $('#dgFeedBackType').datagrid('reload');
                } else {
                    alert(html);
                }
            }
        });
    }

</script>