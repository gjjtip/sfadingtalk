<%--
  User: val.jzp
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <title>反馈类型查询</title>
    <jsp:include page="/include/common.jsp"/>
    <script type="text/javascript" src="<c:url value="/resources/lib/jquery/plugin/jquery.form.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/lib/My97DatePicker/WdatePicker.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/verify.js" />"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#dgFeedBackType').datagrid({
                url: '<c:url value="/web/admin/feedBackType/getFeedBackTypeListJSON.action" />',
                title: '反馈类型查询',
                singleSelect: true,
                fit: true,
                fitColumns: true,
                toolbar: '#tFeedBackType',
                rownumbers: true,
                pagination: true,
                pageSize: 20,
                columns: [[
                    {field: 'id', title: '编号', width: 20, align: 'center'},
                    {field: 'problemName', title: '反馈类型', width: 90, align: 'center'},
                    {
                        field: 'edit', title: '操作', width: 30, align: 'center', formatter: function (val, row) {
                        if (row.id < 0) {
                            return '';
                        }
                        var editUrl = "<c:url value="/web/admin/feedBackType/getFeedBackTypeEditPage.action?feedBackTypeId=" />" + row.id;
                        return '&nbsp;' +
//                                '<a href="javascript:void(0)" onclick="openWindow(\'#wProblemBackType\', \'' + viewUrl + '\')">详情</a>' +
//                                '&nbsp;|&nbsp;' +
                                '<a href="javascript:void(0)" onclick="openWindow(\'#wFeedBackType\', \'' + editUrl + '\')">编辑</a>' +
                                '&nbsp;|&nbsp;' +
                                '<a href="javascript:void(0)" onclick="submitRemove(\'' + row.id + '\', \'1\')">删除</a>' +
                                '&nbsp;';
                    }
                    }
                ]]
            });

            formatPagination("dgFeedBackType");

            vEasyUIUtil.createWindow("wFeedBackType", {
                title: '新增/编辑',
                width: 640,
                height: vSugar.getMaxWinHeight("mainPanel", 600),
                modal: true,
                closed: true,
                iconCls: 'icon-save'
            });
            vEasyUIUtil.createWindow("wAdminUser2", {
                width: '49%',
                height: vSugar.getMaxWinHeight("mainPanel", 600),
                modal: true,
                maximized: false,
                closed: true,
                collapsible: false,
                minimizable: false,
                maximizable: true,
                iconCls: 'icon-save'
            });
            $("#wAdminUser2").window("body").css("padding", "0");

            $("#winImport").window({
                title: '导入数据',
                width: 360,
                height: 180,
                modal: true,
                closed: true,
                iconCls: 'icon-save'
            });

            $("#uploadFile").validatebox({
                required: true,
                validType: "verifyFileType[[/.xls$/], '仅允许.xls格式文件上传']",
                missingMessage: "请上传文件"
            });
        });

        /******************************* 数据操作 *******************************/

        function submitRemove(id) {
            if (!confirm("确认删除")) {
                return;
            }
            // isFakeDelete=1表示假删除
            $.post("<c:url value="/web/admin/feedBackType/logicRemoveFeedBackType.action"/>", {id: id},
                    function (result) {
                        if (result == 1) {
                            $('#dgFeedBackType').datagrid('reload');
                        }
                    }
            );
        }

        function submitSearch() {
            var dgFeedBackType = $('#dgFeedBackType');

            dgFeedBackType.datagrid('options').pageNumber = 1;
            dgFeedBackType.datagrid('getPager').pagination("options").pageNumber = 1;
            var param = {
                problemName: $("#problemName").val()
            };
            dgFeedBackType.datagrid({queryParams: param}, 'reload');
        }


        function clearSearch() {
        }


        function submitImportForm() {

            var flag = true;
            $(".validateNeed1").each(function () {
                if (!$(this).validatebox("isValid")) {
                    flag = false;
                }
            });

            if (!flag) {
                return false;
            }

            $.messager.progress({
                title: '请稍等',
                msg: '数据加载中....'
            });

            $("#importForm").ajaxSubmit({
                success: function (html, status) {
                    $("#winImport").window('close');
                    $.messager.progress('close');
                    if (status == "success" && html == "success") {
                        alert("Excel正在导入,请到导入结果中查询状态");
                        $('#dgFeedBackType').datagrid('reload');
                    } else {
                        alert("服务器异常或导入了空excel，请重新尝试");
                    }
                },
                error: function () {
                    $.messager.progress('close');
                    alert("出错了，反馈类型未知，请联系管理员");
                }
            });
            return true;
        }

    </script>
</head>
<body class="easyui-layout">
<div data-options="region:'center',border:false" class="mainPanel">
    <div id="tFeedBackType" class="toolBarPadding">
        <div style="float:left;">

            <a href="javascript:void(0)" class="easyui-linkbutton v-align-middle"
               data-options="iconCls:'icon-add',plain:true"
               onclick="openWindow('#wFeedBackType', '<c:url
                       value="/web/admin/feedBackType/getFeedBackTypeEditPage.action"/>')">新增&nbsp;</a>
        </div>
        <div style="float:right;">
            <div>
                <label for="problemName" class="v-align-middle" style="width:160px;">反馈类型：</label>
                <input type="text" class="easyui-textbox" id="problemName" name="problemName" value=""
                       style="width:160px;"/>
                &nbsp;
                <a href="javascript:void(0)" class="easyui-linkbutton v-align-middle"
                   data-options="iconCls:'icon-search'"
                   onclick="submitSearch()">搜索&nbsp;</a>
            </div>
        </div>
        <div style="float:right;">
        </div>
        <div style="clear:both;"></div>
    </div>
    <table id="dgFeedBackType"></table>
</div>


</body>
</html>