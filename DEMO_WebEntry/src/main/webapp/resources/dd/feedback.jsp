<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>问题反馈</title>
</head>
<!--移动端自适应-->
<script src="<c:url value="/resources/dd/js/flexible.js" />"></script>
<!--config配置文件-->
<script src="<c:url value="/resources/js/urlConfig.js" />"></script>
<!--自定义css-->
<link href="<c:url value="/resources/dd/css/flexible.css" />" rel="stylesheet">
<link href="<c:url value="/resources/dd/css/feedback.css" />" rel="stylesheet">

<script src="<c:url value="/resources/lib/jquery/jquery-1.11.3.min.js" />"></script>


<body>
<div style="display:none;"><input type="text" id="dutyType" name="dutyType" value="${dutyType}"></div>
<div style="display:none;"><input type="text" id="userName" name="userName" value="${userName}"></div>
<div style="display:none;"><input type="text" id="userTel" name="userTel" value="${userTel}"></div>
<div style="display:none;"><input type="text" id="exceptionId" name="exceptionId" value="${exceptionId}"></div>

<seciton class="shop">
    <div class="name scale-1px">
        <label for="storeName">门店<input id="storeName" name="storeName" placeholder="请输入门店名称" type="text"
                                        value="${storeName}"></label>
    </div>

    <div class="type scale-1px">
        <label for="feedbackType">反馈类型
            <select id="feedbackType" name="feedbackType" value="${type}">
                <option value="">--请选择--</option>
                <c:forEach items="${feedBackTypeList}" var="checkStatus">
                    <option value="${checkStatus.problemName}"
                            <c:if test="${checkStatus==feedBackType.problemName}">selected="selected"</c:if>>${checkStatus.getProblemName()}</option>
                </c:forEach>
            </select>
        </label>
    </div>
</seciton>

<div class="space"></div>
<seciton class="describe">
    <textarea placeholder="描述" name="describe" id="describe" cols="30" rows="10">${describe}</textarea>
</seciton>

<div class="button">
    <button id="submitFeedback">提交</button>
</div>
</body>

<script>
    document.querySelector('#submitFeedback').onclick = function () {
        $.ajax({
            url: urlConfig + "/access/feedback/submit.action",
            data: {
                dutyType: $("#dutyType").val(),
                feedbackType: $("#feedbackType").val(),
                userName: $("#userName").val(),
                userTel: $("#userTel").val(),
                describe: $("#describe").val(),
                storeName: $("#storeName").val(),
                exceptionId: $("#exceptionId").val()
            },
            type: 'POST',
            success: function (data) {
                if (data == "success") {
                    alert("问题反馈成功");
                    window.location = urlConfig + "/access/home.action";
                } else {
                    alert(data);
                }
            },
            error: function (xhr, errorType, error) {

            }
        });
    };
</script>
<script>
    $("#feedbackType").prop("value", "${type}");
</script>
</html>
