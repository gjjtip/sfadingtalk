<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>签到列表</title>
</head>
<!--移动端自适应-->
<script src="<c:url value="/resources/dd/js/flexible.js" />"></script>
<!--时钟-->
<link href="<c:url value="/resources/dd/css/style2.css" />" rel="stylesheet">
<script src="<c:url value="/resources/dd/js/moment.min.js" />"></script>
<!--config配置文件-->
<script src="<c:url value="/resources/js/urlConfig.js" />"></script>
<!--基础JS-->
<link href="<c:url value="/resources/lib/bootstrap-3.3.1-dist/css/bootstrap.min.css" />" rel="stylesheet">
<!--自定义css-->
<link href="<c:url value="/resources/dd/css/flexible.css" />" rel="stylesheet">
<link href="<c:url value="/resources/dd/css/feld_sign.css" />" rel="stylesheet">

<body class="sign">
<section class="clock">
    <div id="clock" class="light">
        <div class="display">
            <div class="date"></div>
            <div class="digits"></div>
        </div>
        <div id="intoStore" class="buttons">点我签到</div>
    </div>
</section>

<section class="shop">
    <div class="top">
    </div>
    <ul>
    </ul>
</section>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <img src="" alt="">
        </div>
    </div>
</div>
<!--基础JS-->
<script src="<c:url value="/resources/lib/jquery/jquery-1.11.3.min.js" />"></script>
<script src="<c:url value="/resources/lib/bootstrap-3.3.1-dist/js/bootstrap.min.js" />"></script>
<!--钉钉JS-->
<script src="http://g.alicdn.com/dingding/open-develop/1.1.9/dingtalk.js"></script>
<script>
    dd.config({
        agentId: '${agentId}', // 必填，微应用ID
        corpId: '${corpId}',//必填，企业ID
        timeStamp: ${timeStamp}, // 必填，生成签名的时间戳
        nonceStr: '${nonceStr}', // 必填，生成签名的随机串
        signature: '${signature}', // 必填，签名
        type: 0,   //选填。0表示微应用的jsapi,1表示服务窗的jsapi。不填默认为0。该参数从dingtalk.js的0.8.3版本开始支持
        jsApiList: ['runtime.permission.requestAuthCode', 'biz.map.view'] // 必填，需要使用的jsapi列表，注意：不要带dd。
    });
</script>
<script type="text/javascript">
    dd.ready(function () {
        dd.runtime.permission.requestAuthCode({
            corpId: '${corpId}',
            onSuccess: function (info) {
                var code = info.code;
                $.ajax({
                    url: urlConfig + "/access/getList.action",
                    data: {
                        accessToken: '${accessToken}',
                        code: code
                    },
                    type: 'GET',
                    success: function (res, status, xhr) {
                        var html = '';
                        res.jsArray.forEach(function (item, index) {
                            console.log(item)
                            html += '<li class="scale-1px"><div class="left"><div class="logo"><img src="./img/logo.jpg" alt=""></div><p>' + item.shopName + '</p></div>'
                            html += '<div class="right">'
                            if (!item.inExceptionId == '') {
                                // 进店有异常
                                html += '<div class="abnormal scale-1px"><button class="scale4-1px-red exceptionInfo"  data-exception= "' + item.inExceptionId + '">异常</button></div>'
                            } else {
                                // 进店正常
                                html += '<div class="normal scale-1px">到店：' + item.inDutyTime + '<img class="myimg" data-src="' + urlConfig + '/' + item.inDutyPhoto + '" src="' + urlConfig + '/resources/dd/img/img3.jpg" alt=""><img class="locationIn" data-latitude="' + item.inLatitude + '" data-longitude="' + item.inLongitude + '" data-shopName="' + item.shopName + '" src="' + urlConfig + '/resources/dd/img/img4.jpg" alt=""></div>'
                            }
                            if (!item.outExceptionId == '') {
                                // 出店有异常
                                html += '<div class="abnormal"><button class="scale4-1px-red exceptionInfo" data-exception= "' + item.outExceptionId + '" >异常</button></div>'
                            } else if (item.outDutyTime == '') {
                                //离店时间为空
                                html += '<div class="abnormal"><button class="scale4-1px-blue" id="outStore">离店签到</button></div>'
                            }
                            else {
                                // 出店正常
                                html += '<div class="normal">离店：' + item.outDutyTime + '<img class="myimg" data-src="' + urlConfig + '/' + item.outDutyPhoto + '" src="' + urlConfig + '/resources/dd/img/img3.jpg" alt=""><img class="locationOut" data-latitude="' + item.outLatitude + '" data-longitude="' + item.outLongitude + '" data-shopName="' + item.shopName + '" src="' + urlConfig + '/resources/dd/img/img4.jpg" alt=""></div>'
                            }
                            html += '</div>'

                        })
                        $('.shop ul').html(html);
                        $('.top').html('<span class="split"></span><p>今天已去过' + res.jsArray.length + '个门店</p>')


                    },
                    error: function (xhr, errorType, error) {
                        logger.e("yinyien:" + '${corpId}');
                        alert(errorType + ', ' + error);
                    }
                });
            },
            onFail: function (err) {
                alert('fail: ' + JSON.stringify(err));
            }
        })
        ;
    });
    dd.error(function (err) {
        alert('dd error: ' + JSON.stringify(err));
    });
</script>
<script>

    //绑定事件
    $('body').on('click', '.myimg', function () {
//        alert($(this)[0].getAttribute('data-src'))
        $('.modal').find('img')[0].src = $(this)[0].getAttribute('data-src')
        $('.modal').modal('show')
    })

    $('body').on('click', '.modal-backdrop', function () {
        $('.modal').modal('hide')
    })

    $('body').on('click', '.locationIn', function () {
        dd.biz.map.view({
            latitude: $(this)[0].getAttribute('data-latitude'), // 纬度
            longitude: $(this)[0].getAttribute('data-longitude'), // 经度
            title: $(this)[0].getAttribute('data-shopName') // 地址/POI名称
        })
    })

    $('body').on('click', '.locationOut', function () {
        dd.biz.map.view({
            latitude: $(this)[0].getAttribute('data-latitude'), // 纬度
            longitude: $(this)[0].getAttribute('data-longitude'), // 经度
            title: $(this)[0].getAttribute('data-shopName') // 地址/POI名称
        })
    })

    $(function () {
        var clock = $('#clock');
        //定义数字数组0-9
        var digit_to_name = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
        //定义星期
        var weekday = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];
        var digits = {};
        //定义时分秒位置
        var positions = [
            'h1', 'h2', ':', 'm1', 'm2', ':', 's1', 's2'
        ];
        //构建数字时钟的时分秒
        var digit_holder = clock.find('.digits');
        $.each(positions, function () {
            if (this == ':') {
                digit_holder.append('<div class="dots">');
            }
            else {
                var pos = $('<div>');
                for (var i = 1; i < 8; i++) {
                    pos.append('<span class="d' + i + '">');
                }
                digits[this] = pos;
                digit_holder.append(pos);
            }
        });
        // 让时钟跑起来
        (function update_time() {
            //调用moment.js来格式化时间
            var now = moment().format("HHmmss");
            digits.h1.attr('class', digit_to_name[now[0]]);
            digits.h2.attr('class', digit_to_name[now[1]]);
            digits.m1.attr('class', digit_to_name[now[2]]);
            digits.m2.attr('class', digit_to_name[now[3]]);
            digits.s1.attr('class', digit_to_name[now[4]]);
            digits.s2.attr('class', digit_to_name[now[5]]);
            var date = moment().format("YYYY年MM月DD日");
            var week = weekday[moment().format('d')];
            $(".date").html(date + ' ' + week);
            // 每秒钟运行一次
            setTimeout(update_time, 1000);
        })()
    })
    document.querySelector('#intoStore').onclick = function () {
        window.location = urlConfig + "/access/in.action";
    }

    $('body').on('click', '#outStore', function () {
        window.location = urlConfig + "/access/out.action";
    })

    $('body').on('click', '.exceptionInfo', function () {
        window.location = urlConfig + "/access/feedback/query.action?exceptionId=" + $(this)[0].getAttribute('data-exception');
    })

</script>
</body>
</html>