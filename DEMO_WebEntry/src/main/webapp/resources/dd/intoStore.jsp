<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>进店签到</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no, width=device-width">

    <link href="<c:url value="/resources/lib/bootstrap-3.3.1-dist/css/bootstrap.min.css" />" rel="stylesheet">
</head>
<!--移动端自适应-->
<script src="<c:url value="/resources/dd/js/flexible.js" />"></script>
<!--地图样式-->
<link href="<c:url value="http://cache.amap.com/lbs/static/main1119.css" />" rel="stylesheet">
<!--高德地图-->
<script src="http://cache.amap.com/lbs/static/es5.min.js"></script>
<script src="http://webapi.amap.com/maps?v=1.3&key=cdb1b0dc5681eb2de7e78928ad2cfc60"></script>
<script type="text/javascript" src="http://cache.amap.com/lbs/static/addToolbar.js"></script>
<!--config配置文件-->
<script src="<c:url value="/resources/js/urlConfig.js" />"></script>
<!--时钟-->
<%--<link href="<c:url value="/resources/dd/css/style2.css" />" rel="stylesheet">--%>
<%--<script src="<c:url value="/resources/dd/js/moment.min.js" />"></script>--%>
<!--基础JS-->
<script src="<c:url value="/resources/lib/jquery/jquery-1.11.3.min.js" />"></script>
<script src="<c:url value="/resources/lib/bootstrap-3.3.1-dist/js/bootstrap.min.js" />"></script>
<!--钉钉JS-->
<script src="http://g.alicdn.com/dingding/open-develop/1.1.9/dingtalk.js"></script>
<!--进店页面样式-->
<link href="<c:url value="/resources/dd/css/flexible.css" />" rel="stylesheet">
<link href="<c:url value="/resources/dd/css/sign.css" />" rel="stylesheet">
<body>

<div id="container1"
     style="width:100%;height:400px;border-color: #0070a9;border:1px solid #b3b3b3;margin-top: 20px"></div>
<section class="sign">
    <div class="name scale-1px">
        <label for="store">门店<input id="store" placeholder="请输入门店名称" type="text"></label>
    </div>
    <div class="photo">
        <img src="/resources/dd/img/img2.jpg" id="v_photo" alt="" onclick="chooseImage()">
    </div>
    <div class="sign_button">
        <button id="submitOnDuty">签到</button>
        <p>如无法签到,请 <a href="./feedback.html" onclick="feedback()">反馈问题</a></p>
    </div>

</section>
<div>

    <div style="display:none;"><input type="text" id="photo" name="photo"></div>
    <div style="display:none;"><input type="text" id="longitude" name="longitude"></div>
    <div style="display:none;"><input type="text" id="latitude" name="latitude"></div>
    <div style="display:none;"><input type="text" id="userName" name="userName"></div>
    <div style="display:none;"><input type="text" id="userTel" name="userTel"></div>

</div>

</body>
<script>
    dd.config({
        agentId: '${agentId}', // 必填，微应用ID
        corpId: '${corpId}',//必填，企业ID
        timeStamp:${timeStamp}, // 必填，生成签名的时间戳
        nonceStr: '${nonceStr}', // 必填，生成签名的随机串
        signature: '${signature}', // 必填，签名
        type: 0,   //选填。0表示微应用的jsapi,1表示服务窗的jsapi。不填默认为0。该参数从dingtalk.js的0.8.3版本开始支持
        jsApiList: ['runtime.info', 'biz.contact.choose', 'runtime.permission.requestAuthCode',
            'device.notification.confirm', 'device.notification.alert', 'device.notification.showPreloader', 'dd.device.notification.hidePreloader',
            'device.notification.prompt', 'biz.ding.post', 'biz.user.get', 'device.geolocation.get', 'biz.map.locate', 'biz.util.uploadImageFromCamera',
            'biz.map.view',
            'biz.util.openLink'] // 必填，需要使用的jsapi列表，注意：不要带dd。
    });
</script>
<script type="text/javascript">
    dd.ready(function () {

        dd.runtime.permission.requestAuthCode({
            corpId: '${corpId}',
            onSuccess: function (info) {
                var code = info.code;
                $.ajax({
                    url: urlConfig + "/access/user.action",
                    data: {
                        accessToken: '${accessToken}',
                        code: code
                    },
                    type: 'GET',
                    success: function (data, status, xhr) {
//                        var info = JSON.parse(data);
                        $("#userName").val(data.name);
                        $("#userTel").val(data.mobile);

                    },
                    error: function (xhr, errorType, error) {
                        logger.e("yinyien:" + '${corpId}');
                        alert(errorType + ', ' + error);
                    }
                });

            },
            onFail: function (err) {
                alert('fail: ' + JSON.stringify(err));
            }
        });

        var longitude;
        var latitude;
        dd.device.geolocation.get({
            targetAccuracy: 50,
            coordinate: 1,
            withReGeocode: Boolean,
            onSuccess: function (result) {
                longitude = result.longitude;
                latitude = result.latitude;
                $("#longitude").val(longitude);
                $("#latitude").val(latitude);
                var map = new AMap.Map("container1", {
                    resizeEnable: true,
                    center: [longitude, latitude],//地图中心点
                    zoom: 13 //地图显示的缩放级别
                });
                //添加点标记，并使用自己的icon
                new AMap.Marker({
                    map: map,
                    position: [longitude, latitude],
                    icon: new AMap.Icon({
                        size: new AMap.Size(40, 50),  //图标大小
                        image: "/resources/dd/img/img5.png"
                    })
                });
            },
            onFail: function (err) {
            }
        });

    });


    function chooseImage() {
        dd.biz.util.uploadImageFromCamera({
            compression: true,
            onSuccess: function (re) {
                $("#v_photo").attr("src", re);
                $("#photoDiv").show();
                $("#photo").val(re);
            },
            onFail: function (err) {
            }
        })
    }


    document.querySelector('#submitOnDuty').onclick = function () {
        $.ajax({
            url: urlConfig + "/access/attend/submit.action?dutyType=in",
            data: {
                photo: $("#photo").val(),
                longitude: $("#longitude").val(),
                latitude: $("#latitude").val(),
                userName: $("#userName").val(),
                userTel: $("#userTel").val(),
                store: $("#store").val()
            },
            type: 'POST',
            success: function (result) {
                if (result == "success") {
                    alert("签到成功");
                    window.location = urlConfig + "/access/home.action";
                } else {
                    alert(result);
                }

            },
            error: function (xhr, errorType, error) {

            }
        });
    };

    function feedback() {
        window.location = urlConfig + "/access/feedback/query.action?dutyType=in&userTel=" + $("#userTel").val() + "&userName=" + $("#userName").val();
    }
</script>
</html>
